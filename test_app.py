from app import Solution

def test_app():
	sol = Solution()

	assert sol.climbStairs(2) == 2
	assert sol.climbStairs(3) == 3
	assert sol.climbStairs(10) == 89